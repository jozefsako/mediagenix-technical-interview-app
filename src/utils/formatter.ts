import { MediagenixEvent } from "../models/form";

/**
 * Replace all uppercase letters with a space followed by the uppercase letter
 * Capitalize the first letter of the string and return the result
 * @param input
 * @returns
 */
export function formatCamelCase(input: string): string {
  const result = input.replace(/([A-Z])/g, " $1");
  return result.charAt(0).toUpperCase() + result.slice(1);
}

export function filterEventByTitleOrDescription(
  events: MediagenixEvent[],
  searchValue: string
) {
  const result: MediagenixEvent[] = [];
  events.forEach((event: MediagenixEvent) => {
    if (
      event.title.includes(searchValue) ||
      event.description.includes(searchValue)
    ) {
      result.push(event);
    }
  });
  return result;
}
