import { StaticFormPage, AutoFrom, TypescriptTest } from "../pages";

interface NavigationProps {
  path: string;
  label: string;
  component: React.ComponentType<any>;
}

export const navigationMapper = {
  StaticFormPage: StaticFormPage,
  AutoForm: AutoFrom,
  TypescriptTest: TypescriptTest,
};

export const navigation: NavigationProps[] = [
  {
    path: "/",
    label: "StaticForm",
    component: navigationMapper.StaticFormPage,
  },
  {
    path: "/auto-form",
    label: "AutoForm",
    component: navigationMapper.AutoForm,
  },
  {
    path: "/typescript-test",
    label: "TypescriptTest",
    component: navigationMapper.TypescriptTest,
  },
];
