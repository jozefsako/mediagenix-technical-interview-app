import { Header } from "./components";
import { WrapperApp } from "./styles/Common.styles";
import logo from "./assets/logo.svg";
import "./styles/App.css";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();

function App() {
  return (
    <div className="App">
      <QueryClientProvider client={queryClient}>
        <WrapperApp>
          <Header logo={logo} />
        </WrapperApp>
      </QueryClientProvider>
    </div>
  );
}

export default App;
