import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import { Nav, Ul } from "../../styles/Common.styles";
import { navigation } from "../../utils/navigation";

interface HeaderProps {
  logo: string;
}

export const Header = ({ logo }: HeaderProps) => {
  return (
    <Router>
      <header className="App-header">
        <Nav>
          <Ul>
            {navigation.map((nav, idx) => (
              <li key={idx}>
                <Link to={nav.path}>{nav.label}</Link>
              </li>
            ))}

            <li>
              <img src={logo} className="App-logo" alt="logo" />
            </li>
          </Ul>
        </Nav>
      </header>

      <Switch>
        {navigation.map((route, idx) => (
          <Route
            key={idx}
            path={route.path}
            exact
            component={route.component}
          />
        ))}
      </Switch>
    </Router>
  );
};
