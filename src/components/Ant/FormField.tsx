import { Form, Input, DatePicker, Select } from "antd";
import { PropsWithChildren } from "react";
import { Field } from "../../models/form";

export interface FormItemProps extends PropsWithChildren {
  field: Field;
}

export const FormItem = ({ field, children }: FormItemProps) => {
  return (
    <Form.Item
      key={field.name.toString()}
      label={field.label}
      name={field.name}
      rules={[{ required: field.required }]}
    >
      {children}
    </Form.Item>
  );
};

export const InputItem = ({ field }: FormItemProps) => (
  <FormItem field={field} children={<Input />} />
);

export const RangePickerItem = ({ field }: FormItemProps) => (
  <FormItem field={field} children={<DatePicker.RangePicker />} />
);

export const TextAreaItem = ({ field }: FormItemProps) => (
  <FormItem field={field} children={<Input.TextArea />} />
);

export const SelectItem = ({ field }: FormItemProps) => (
  <FormItem field={field} children={<Select options={field.options} />} />
);
