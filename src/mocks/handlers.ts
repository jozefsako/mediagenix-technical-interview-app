import { rest } from "msw";
import { filterEventByTitleOrDescription } from "../utils/formatter";
import { dataSample } from "./sample";

export const handlers = [
  rest.get("/events", (req, res, ctx) => {
    console.log("[GET]: intercepted");
    return res(ctx.json(dataSample));
  }),

  rest.get("/events/search", (req, res, ctx) => {
    console.log("[GET]: intercepted");
    const query = req.url.searchParams.get("q");

    if (!query) {
      return res(ctx.status(200), ctx.json(dataSample));
    }

    const searchResult = filterEventByTitleOrDescription(dataSample, query);
    return res(ctx.status(200), ctx.json(searchResult));
  }),

  rest.post("/events", async (req, res, ctx) => {
    console.log("[POST]: intercepted");
    const eventData = await req.json();
    return res(ctx.status(201), ctx.json(eventData));
  }),
];
