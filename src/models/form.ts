export interface FieldOption {
  label: string;
  value: string;
}

export type FieldComponentType =
  | "text"
  | "select"
  | "range_picker"
  | "textarea";

export interface Field {
  name: string | string[];
  label: string;
  component: FieldComponentType;
  options?: FieldOption[];
  required?: boolean;
}

export interface MediagenixEvent {
  id: string;
  title: string;
  type: string;
  startDate: string;
  endDate: string;
  description: string;
}

export interface ColumnField {
  title: string;
  dataIndex: string;
  key: string;
}

export interface Data {
  id: string;
  [key: string]: string;
}
