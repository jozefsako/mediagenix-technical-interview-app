import axios from "axios";
import { MediagenixEvent } from "../models/form";

export const baseUrl = "https://my-api-dummy.com";

export const createEvent = async (eventData: MediagenixEvent) => {
  try {
    const response = await axios.post(`${baseUrl}/events`, eventData);
    return response.data;
  } catch (error) {
    console.error(error);
  }
};

export const getAllEvents = async () => {
  try {
    const response = await axios.get(`${baseUrl}/events`);
    return response.data;
  } catch (error) {
    console.error(error);
  }
};

export const updateEvent = async (
  eventId: string,
  eventData: MediagenixEvent
) => {
  try {
    const response = await axios.put(`${baseUrl}/events/${eventId}`, eventData);
    return response.data;
  } catch (error) {
    console.error(error);
  }
};

export const deleteEvent = async (eventId: string) => {
  try {
    await axios.delete(`${baseUrl}/events/${eventId}`);
  } catch (error) {
    console.error(error);
  }
};
