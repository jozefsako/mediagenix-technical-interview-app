import { Form } from "antd";
import {
  InputItem,
  SelectItem,
  RangePickerItem,
  TextAreaItem,
} from "../../components";
import { Field, ColumnField, MediagenixEvent } from "../../models/form";
import { formatCamelCase } from "../../utils/formatter";
import { DynamicFormProps } from "./AutoForm";
import { Container } from "./autoForm.styles";

export const getFormItems = (schema: Field[]) => {
  return schema.map((field, idx) => {
    switch (field.component) {
      case "text":
        return <InputItem key={idx} field={field} />;
      case "select":
        return <SelectItem key={idx} field={field} />;
      case "range_picker":
        return <RangePickerItem key={idx} field={field} />;
      case "textarea":
        return <TextAreaItem key={idx} field={field} />;
      default:
        return null;
    }
  });
};

export const formatRawDataIntoEvent = (rawData: any) => {
  const rangePicker = rawData["startDate"]["endDate"];

  return {
    id: rawData["id"],
    title: rawData["title"],
    type: rawData["type"],
    startDate: rangePicker[0].format("YYYY-MM-DD"),
    endDate: rangePicker[1].format("YYYY-MM-DD"),
    description: rawData["description"],
  } as MediagenixEvent;
};

export const DynamicForm = ({
  schema,
  form,
  onFinish,
  onFinishFailed,
}: DynamicFormProps) => {
  return (
    <Container>
      <Form
        name="basic"
        labelCol={{ span: 5 }}
        className="form"
        form={form}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        {getFormItems(schema)}
      </Form>
    </Container>
  );
};

export const getColumnsFromSchema = (schema: Field[]) => {
  const columns: ColumnField[] = [];

  schema.forEach((field) => {
    if (field.component === "range_picker") {
      const rangePickerFields = field.name as string[];
      rangePickerFields.forEach((rangePickerItem) => {
        columns.push({
          title: formatCamelCase(rangePickerItem),
          dataIndex: rangePickerItem,
          key: rangePickerItem,
        });
      });
    } else {
      columns.push({
        title: field.label,
        dataIndex: field.name as string,
        key: field.name.toString(),
      });
    }
  });

  return columns;
};
