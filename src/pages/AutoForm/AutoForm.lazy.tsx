import React, { Suspense } from "react";
import { Loading } from "../../components/Loading/Loading";

const LazyAutoForm = React.lazy(() => import("./AutoForm"));

function AutoForm() {
  return (
    <Suspense fallback={<Loading />}>
      <LazyAutoForm />
    </Suspense>
  );
}

export default AutoForm;
