import { MediagenixEvent } from "../../models/form";

export const mapEvents = (events: MediagenixEvent[]) => {
  return events.map((item: MediagenixEvent, idx: number) => {
    return {
      key: idx,
      ...item,
    };
  });
};
