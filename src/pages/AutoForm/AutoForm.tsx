import { useEffect, useState } from "react";
import { useMutation, useQuery } from "react-query";

import { Table, Form, Button, Space, FormInstance, Input } from "antd";
import Modal from "antd/es/modal/Modal";

import { schema } from "./../../models/tableSchema";
import { Container } from "./autoForm.styles";
import { Field, MediagenixEvent } from "../../models/form";
import { fields } from "./constants";
import { DynamicForm, getColumnsFromSchema } from "./AutoForm.Components";
import { fetchEvents, saveEvent, searchEvent } from "./AutoForm.service";

export interface DynamicFormProps {
  schema: Field[];
  form: FormInstance<any>;
  onFinishFailed?: (errorInfo: any) => void;
  onFinish?: (values: any) => void;
}

const AutoForm = () => {
  const [events, setEvents] = useState<MediagenixEvent[]>([]);
  const [open, setOpen] = useState(false);
  const [search, setSearch] = useState("");
  const [form] = Form.useForm();
  const columns = getColumnsFromSchema(schema);

  const {
    data: fetchedEvents,
    isLoading: eventsLoading,
    isError: eventsError,
  } = useQuery("events", fetchEvents);

  useEffect(() => {
    if (fetchedEvents) {
      setEvents(fetchedEvents);
    }
  }, [fetchedEvents]);

  const showModal = () => {
    setOpen(true);
  };

  const addEventMutation = useMutation(saveEvent, {
    onSuccess: (newEvent: MediagenixEvent) => {
      setEvents((prevEvents) => [
        ...prevEvents,
        {
          ...newEvent,
          key: prevEvents.length,
        },
      ]);
    },
    onError: (error) => {
      console.error(error);
    },
  });

  const handleOk = () => {
    form
      .validateFields()
      .then((values) => {
        addEventMutation.mutate({ ...values, id: events.length });
        form.resetFields();
      })
      .catch((info) => {
        console.log("Validate Failed:", info);
      });
  };

  const handleCancel = () => {
    form.resetFields();
    setOpen(false);
  };

  const onSearch = (data: string) => {
    console.log("search data => ", data);
    searchEvent(data).then((data) => setEvents(data));
  };

  if (eventsLoading) {
    return <div>Loading events...</div>;
  }

  if (eventsError) {
    return <div>Error fetching events</div>;
  }

  return (
    <Container>
      <Space className="header">
        <Input.Search
          placeholder="Search events"
          allowClear
          enterButton="Search"
          size="large"
          onSearch={onSearch}
        />
        <Button type="primary" onClick={showModal}>
          {fields.buttons.add}
        </Button>
      </Space>
      {open && (
        <Modal
          title={fields.modal.title}
          open={open}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          <DynamicForm schema={schema} form={form} />
        </Modal>
      )}
      <Table columns={columns} dataSource={events} />
    </Container>
  );
};

export default AutoForm;
