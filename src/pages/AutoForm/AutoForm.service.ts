import axios from "axios";
import { MediagenixEvent } from "../../models/form";
import { formatRawDataIntoEvent } from "./AutoForm.Components";
import { mapEvents } from "./AutoForm.mapper";

export async function fetchEvents() {
  const response = await axios.get("/events");
  return mapEvents(response.data);
}

export async function searchEvent(searchValue: string) {
  const response = await axios.get(`/events/search?q=${searchValue}`);
  return mapEvents(response.data);
}

export async function saveEvent(eventData: MediagenixEvent) {
  const response = await axios.post(
    "/events",
    formatRawDataIntoEvent(eventData)
  );
  return response.data as MediagenixEvent;
}
