import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: white !important;
  padding: 20px;
  border-radius: 20px;

  .form {
    width: 100%;
    max-width: 600px;
  }

  .header {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 20px;
    width: 100%;
  }

  svg {
    height: max-content !important;
  }
`;
