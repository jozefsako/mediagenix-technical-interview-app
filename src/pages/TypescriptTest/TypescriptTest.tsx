import { Container } from "./typescriptTest.styles";

type First<T extends any[]> = T;
type arr1 = ["a", "b", "c"];
type arr2 = [3, 2, 1];
type head1 = First<arr1>; // expected to be 'a'
type head2 = First<arr2>; // expected to be 3
const arr1Value: head1 = ["a", "b", "c"];
const arr2Value: head2 = [3, 2, 1];

const TypescriptTest = () => {
  return (
    <Container>
      <p>
        head1 : {arr1Value[0]} type == {typeof arr1Value[0]}
      </p>
      <p>
        head2 : {arr2Value[0]} type == {typeof arr2Value[0]}
      </p>
    </Container>
  );
};

export default TypescriptTest;
