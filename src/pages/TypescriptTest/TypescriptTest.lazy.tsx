import React, { Suspense } from "react";
import { Loading } from "../../components/Loading/Loading";

const LazyTsTest = React.lazy(() => import("./TypescriptTest"));

function TypescriptTest() {
  return (
    <Suspense fallback={<Loading />}>
      <LazyTsTest />
    </Suspense>
  );
}

export default TypescriptTest;
