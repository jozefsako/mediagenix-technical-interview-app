export { default as StaticFormPage } from "./StaticForm/StaticForm.lazy";
export { default as AutoFrom } from "./AutoForm/AutoForm.lazy";
export { default as TypescriptTest } from "./TypescriptTest/TypescriptTest.lazy";
