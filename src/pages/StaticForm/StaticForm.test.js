import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import StaticFormPage from "./StaticForm";

window.matchMedia =
  window.matchMedia ||
  function () {
    return {
      matches: false,
      addListener: function () {},
      removeListener: function () {},
    };
  };

const sampleData = [
  {
    name: "Jozef Sako",
    age: 30,
    email: "jozef.sako@gmail.com",
  },
  {
    name: "Jojo",
    age: 21,
    email: "jojo@gmail.com",
  },
];

const sampleColums = [
  {
    title: "Name",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Age",
    dataIndex: "age",
    key: "age",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
];

describe("[StaticFormPage]", () => {
  it("Render: StaticFormPage and expects table to be in the documents", () => {
    const { getByTestId } = render(<StaticFormPage />);
    const table = getByTestId("static-page");
    expect(table).toBeInTheDocument();
  });

  it("Render: the StaticFormPage component with the correct data", () => {
    const { getByTestId } = render(
      <StaticFormPage datasource={sampleData} columns={sampleColums} />
    );

    fireEvent.load(getByTestId("static-page"));
    const tableRows =
      getByTestId("static-page-table").querySelectorAll("tbody tr");

    expect(tableRows).toHaveLength(sampleData.length);

    expect(tableRows[0].querySelector("td:nth-of-type(1)").textContent).toBe(
      sampleData[0].name
    );
    expect(tableRows[0].querySelector("td:nth-of-type(2)").textContent).toBe(
      sampleData[0].age.toString()
    );
    expect(tableRows[0].querySelector("td:nth-of-type(3)").textContent).toBe(
      sampleData[0].email
    );

    expect(tableRows[1].querySelector("td:nth-of-type(1)").textContent).toBe(
      sampleData[1].name
    );
    expect(tableRows[1].querySelector("td:nth-of-type(2)").textContent).toBe(
      sampleData[1].age.toString()
    );
    expect(tableRows[1].querySelector("td:nth-of-type(3)").textContent).toBe(
      sampleData[1].email
    );
  });
});
