import React, { Suspense } from "react";
import { Loading } from "../../components/Loading/Loading";

const LazyStaticFormPage = React.lazy(() => import("./StaticForm"));

function StaticFormPage() {
  return (
    <Suspense fallback={<Loading />}>
      <LazyStaticFormPage />
    </Suspense>
  );
}

export default StaticFormPage;
