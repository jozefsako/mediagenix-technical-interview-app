import React from "react";
import { Table } from "antd";
import { mapEvents } from "../AutoForm/AutoForm.mapper";
import { sampleColumns, dataSource } from "./constants";
import { Container } from "./staticForm.styles";
import { ColumnField } from "../../models/form";

interface StaticFormPageProps {
  datasource?: any;
  columns?: ColumnField[];
}

const StaticFormPage = ({ datasource, columns }: StaticFormPageProps) => {
  const data = mapEvents(dataSource);
  return (
    <Container data-testid="static-page">
      <Table
        data-testid="static-page-table"
        dataSource={datasource ?? data}
        columns={columns ?? sampleColumns}
      />
    </Container>
  );
};

export default StaticFormPage;
